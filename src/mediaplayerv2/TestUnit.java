/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mediaplayerv2;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author Matt
 */
public class TestUnit extends Application{
    
    private MusicEngine testEngine;
    
//    public TestUnit() {
//        testEngine = new MusicEngine();
//    }
    
    private void testMusicEngine() {
        printPlaylist();
        testEngine.addTrack("music/triangle.mp3");
        printPlaylist();
        testEngine.addTrack("music/rubicon.mp3");
        testEngine.addTrack("music/prayers.mp3");
        printPlaylist();
        testEngine.changeVolume(0.9);
        testEngine.play();
        
        
        
        
    }
    
    public static void main(String[] args) {
        Application.launch(TestUnit.class, (java.lang.String[]) null);
    }
    
    private void printPlaylist() {
        System.out.println("Playlist:");
        System.out.println(testEngine.getMusicPlaylist().toString());
    }

    @Override
    public void start(Stage stage) throws Exception {
        testEngine = new MusicEngine();
        testMusicEngine();
    }
}
