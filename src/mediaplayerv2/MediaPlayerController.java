/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mediaplayerv2;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

/**
 * FXML Controller class
 *
 * @author Matt
 */
public class MediaPlayerController extends AnchorPane implements Initializable {

    @FXML
    private Button playButton;
    @FXML
    private Button prevButton;
    @FXML
    private Button nextButton;
    @FXML
    private Button likeButton;
    @FXML
    private ProgressBar playbackProgressBar;
    @FXML
    private Slider volumeSlider;
    @FXML
    private TextArea playlistArea;
    @FXML
    private Label leftTimeLabel;
    @FXML
    private Label rightTimeLabel;
    @FXML
    private MenuItem importSubMenu;

    private Main application;
    private MusicEngine engine;

    public void setApp(Main application) {
        this.application = application;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("Initialize");
        engine = new MusicEngine();
        //engine.addTrack("music/triangle.mp3");

        volumeSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                    Number old_val, Number new_val) {
                engine.changeVolume(new_val.doubleValue() / 10.0);
                System.out.println("Vol: " + new_val.doubleValue());
            }
        });
    }

    @FXML
    private void importTracks(ActionEvent event) {
        FileFilter filter = new Mp3Filter();
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(filter);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        chooser.showOpenDialog(null);
        File soundFile = chooser.getSelectedFile();
        System.out.println("Choosen file: " + soundFile);
        System.out.println(soundFile.isDirectory());
        
        if (soundFile.isDirectory()) {
            File[] musicFiles = soundFile.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".mp3");
                }
            });
            
            for(File f : musicFiles) {
                playlistArea.appendText(f.getName() + "\n");
                engine.addTrack(f.getAbsolutePath());
            }

        }
        else {
            playlistArea.appendText(soundFile.getName() + "\n");
            engine.addTrack(soundFile.getAbsolutePath());
        }

//        String path = "music";
//        File folder = new File(path);
//        File[] musicFiles = folder.listFiles();
//        System.out.println(musicFiles[2]);
//        System.out.println(musicFiles.length);
    }

    @FXML
    private void playToggle(ActionEvent event) {
        engine.play();
    }

    @FXML
    private void prevDrop(ActionEvent event) {
        engine.prev();
    }

    @FXML
    private void nextDrop(ActionEvent event) {
        engine.next();
    }

    @FXML
    private void adjustVolume(ScrollEvent event) {
        //System.out.println(volumeSlider.getValue());

    }

    @FXML
    private void likeTrack(ActionEvent event) {
        engine.like();
    }

    @FXML
    private void addTracks(DragEvent event) {
        System.out.println("Attempint to copy dragged tracks");
        Dragboard db = event.getDragboard();

        if (db.hasFiles()) {
            event.acceptTransferModes(TransferMode.ANY);

            for (File file : db.getFiles()) {
                engine.addTrack(file.getAbsolutePath());
            }
        }
    }

}
