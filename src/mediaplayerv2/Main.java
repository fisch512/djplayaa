/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mediaplayerv2;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Matt
 */
public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        try {
            AnchorPane page = (AnchorPane) FXMLLoader.load(Main.class.getResource("MediaPlayer.fxml"));
            Scene scene = new Scene(page);
            primaryStage.setTitle("Media Player");
            primaryStage.setScene(scene);
            primaryStage.show();
        }
        catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        Application.launch(Main.class, (java.lang.String[]) null);
    }
}
