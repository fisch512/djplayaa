/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mediaplayerv2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Matt
 */
class MusicPlaylist {
    private List<MusicTrack> playlist;
    private int curTrack;
    
    public MusicPlaylist() {
        curTrack = 0;
        playlist = new ArrayList<MusicTrack>();
    }
    
//    public MusicPlaylist(ArrayList<MusicTrack> playlist) {
//        this.playlist = playlist.;
//        curTrack = 0;
//    }
    
    public List<MusicTrack> getPlaylist() {
        return playlist;
    }
    
//    public void setPlaylist(MusicTrack[] playlist) {
//        this.playlist = playlist;
//        curTrack = 0;
//    }
    
    public MusicTrack getTrack(int pos) {
        return playlist.get(pos);
    }
    
    public MusicTrack getTrack() {
        return playlist.get(curTrack);
    }
    
    public MusicTrack getNextTrack() {
        if(curTrack == (playlist.size() - 1)) {
            return null;
        } else {
            curTrack++;
            return playlist.get(curTrack);
        }
    }
    
    public int getLength() {
        return playlist.size();
    }
    
    public void addTrack(MusicTrack track) {
        System.out.println(track.getName());
        playlist.add(track);
    }
    
    public String toString() {
        String r = "";
        for(MusicTrack m : playlist) {
            r += m.getName() + "\n";
        }
        return r;
    }
}
