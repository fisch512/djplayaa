/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mediaplayerv2;

import java.io.File;
import com.echonest.api.v4.EchoNestAPI;
import com.echonest.api.v4.TimedEvent;
import com.echonest.api.v4.Track;
import com.echonest.api.v4.TrackAnalysis;
import com.echonest.api.v4.Playlist;
import com.echonest.api.v4.Song;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/**
 *
 * @author Matt
 */
public class MusicEngine {

    public final static String EN_API_KEY = "I0XRCZOTPDVJNLMS5";

    private MusicPlaylist playlist;
    private MusicTrack track;
    private MediaPlayer player;
    private Media media;
    private EchoNestAPI en;
    private List<TimedEvent> sections;

    private double playBackTime;
    private double volume;

    private int currentTrack;

    private boolean play;

    public MusicEngine() {
        playlist = new MusicPlaylist();
        en = new EchoNestAPI(EN_API_KEY);
        volume = 0.5;
        play = false;
        playBackTime = 0.00;
        currentTrack = 0;
    }

    public void addTrack(String trackURL) {
        System.out.println("MusicEngine.addTrack: started");
        playlist.addTrack(new MusicTrack(en, trackURL));
        System.out.println("Playlist.addTrack: complete");
        if (playlist.getLength() == 1 || (playlist.getLength() - 1) == currentTrack) {
            System.out.println("Loading the first track");
            loadTrack(playlist.getTrack());
        }
    }

    private void loadTrack(MusicTrack track) {
        System.out.println("Attempting to load track");
        this.track = track;
        player = new MediaPlayer(track.getMedia());
        System.out.println("Started MediaPlayer: Success");
        sections = track.getAnalysis().getSections();
        playBackTime = sections.get(0).getStart();
    }

    private void loadNextTrack() {
        currentTrack++;
        MusicTrack temp = playlist.getNextTrack();
        if (temp != null) {
            loadTrack(temp);
        }
        else {
            System.out.println("No next track");
            currentTrack--;
        }
    }

    public void changeVolume(double vol) {
        volume = vol;
        player.setVolume(vol);
    }

    //Play or pause - toggle
    public void play() {
        System.out.println("Attempting to play");
        if (player != null) {
            if (play) {
                play = false;
                player.pause();
            }
            else {
                play = true;
                player.play();
            }
        }
    }

    //Minus two seconds to skip back a drop point
    public void prev() {
        playBackTime = player.currentTimeProperty().get().toSeconds();
        System.out.println(playBackTime);
        for (int i = sections.size() - 1; i >= 0; i--) {
            if (sections.get(i).getStart() < (playBackTime - 2.0)) {
                if (i == 0) {
                    playBackTime = sections.get(0).getStart();
                }
                else {
                    playBackTime = sections.get(i).getStart();
                    player.seek(Duration.seconds(playBackTime));
                    break;
                }
            }
        }
    }

    public void next() {
        playBackTime = player.currentTimeProperty().get().toSeconds();
        System.out.println(playBackTime);
        System.out.println(sections.size());
        System.out.println(sections.toString());
        for (int i = 0; i < sections.size(); i++) {
            if (sections.get(i).getStart() > playBackTime) {
                if (i == sections.size() - 1) {
                    loadNextTrack();
                    play();
                    System.out.println("Trying to load next track");
                }
                else {
                    playBackTime = sections.get(i).getStart();
                    player.seek(Duration.seconds(playBackTime));
                    break;
                }
            }
        }
    }

    public void like() {
        System.out.println("Liked Track: " + track.getName());
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("likes.txt", true)))) {
            out.println(track.getName());
        }
        catch (IOException e) {
            System.err.println(e);
        }
    }

    public MusicPlaylist getMusicPlaylist() {
        return playlist;
    }

}
