/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mediaplayerv2;

import java.io.File;
import javax.swing.filechooser.FileFilter;
import java.io.BufferedInputStream;

/**
 *
 * @author Matt
 */
public class Mp3Filter extends FileFilter {

    //Accept all directories and all gif, jpg, tiff, or png files.
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }

        String extension = Utils.getExtension(f);
        if (extension != null) {
            return extension.equals(Utils.mp3);
        }
        
        return false;
    }

    //The description of this filter
    public String getDescription() {
        return "Just Mp3's";
    }
}
