/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mediaplayerv2;

import com.echonest.api.v4.EchoNestAPI;
import com.echonest.api.v4.EchoNestException;
import com.echonest.api.v4.Track;
import com.echonest.api.v4.TrackAnalysis;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.media.Media;

/**
 *
 * @author Matt
 */
class MusicTrack {

    private final String fileURL;
    private File file;
    private TrackAnalysis analysis;
    private final EchoNestAPI en;
    private Media media;

    public MusicTrack(EchoNestAPI en, String fileURL) {
        this.en = en;
        this.fileURL = fileURL;
        init();
    }

    private void init() {
        file = new File(fileURL);

        if (!file.exists()) {
            System.err.println("Can't find " + file.getName());
        }
        else {
            try {
                System.out.println("File exists");
                String mediaLocation = file.toURI().toString();
                media = new Media(mediaLocation);
                System.out.println("Media created");
                Track track = en.uploadTrack(file);
                track.waitForAnalysis(30000);
                if (track.getStatus() == Track.AnalysisStatus.COMPLETE) {
                    analysis = track.getAnalysis();
                }
//                boolean trackUploaded = false;
//                int maxIter = 50;
//                for (int i = 0; i < maxIter; i++) {
//                    if (track.getStatus() == Track.AnalysisStatus.COMPLETE) {
//                        trackUploaded = true;
//                        analysis = track.getAnalysis();
//                        break;
//                    } else {
//                        try {
//                            Thread.sleep(10);
//                        }
//                        catch (InterruptedException ex) {
//                            Logger.getLogger(MusicTrack.class.getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                }
                
//                if (!trackUploaded) {
//                    System.err.println("Track not uploaded correctly: " + file);
//                }

            }
            catch (EchoNestException | IOException ex) {
                Logger.getLogger(MusicTrack.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String getFileURL() {
        return fileURL;
    }
    
    public Media getMedia() {
        return media;
    }

    public TrackAnalysis getAnalysis() {
        return analysis;
    }
    
    public String getName() {
        return file.toString();
    }
}
