/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mediaplayerv2;

import java.io.File;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matt
 */
public class Mp3FilterTest {

    public Mp3FilterTest() {
    }

    /**
     * Test of accept method, of class Mp3Filter.
     */
    @Test
    public void testAccept() {
        System.out.println("* Mp3FilterJUnit4Test: accept(File)");
        Mp3Filter instance = new Mp3Filter();

        assertTrue(instance.accept(new File("music/test1.mp3")));
        assertTrue(instance.accept(new File("music")));
        assertFalse(instance.accept(new File("music/test4.jpg")));
    }
}
