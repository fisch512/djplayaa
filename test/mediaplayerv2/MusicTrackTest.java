/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mediaplayerv2;

import com.echonest.api.v4.EchoNestAPI;
import com.echonest.api.v4.TrackAnalysis;
import javafx.scene.media.Media;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matt
 */
public class MusicTrackTest {

    private static MusicTrack testTrack;
    private static EchoNestAPI en;

    public MusicTrackTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        en = new EchoNestAPI(MusicEngine.EN_API_KEY);
        testTrack = new MusicTrack(en, "music/test1.mp3");
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getFileURL method, of class MusicTrack.
     */
    @Test
    public void testGetFileURL() {
        System.out.println("* MusicTrackJUnit4Test: getFileURL()");
        assertTrue(testTrack.getFileURL().equals("music/test1.mp3"));
    }

    /**
     * Test of getMedia method, of class MusicTrack.
     */
    @Test
    public void testGetMedia() {
        System.out.println("* MusicTrackJUnit4Test: getGetMedia()");
        assertTrue(testTrack.getMedia() != null);
    }

    /**
     * Test of getAnalysis method, of class MusicTrack.
     */
    @Test
    public void testGetAnalysis() {
        System.out.println("* MusicTrackJUnit4Test: getAnalysis");
        assertTrue(testTrack.getAnalysis() != null);
    }
}