/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mediaplayerv2;

import java.io.File;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matt
 */
public class UtilsTest {
    
    public UtilsTest() {
    }

    /**
     * Test of getExtension method, of class Utils.
     */
    @Test
    public void testGetExtension() {
        System.out.println("* UtilsJUnit4Test: getExtension(File)");
        File f = new File("music/test1.mp3");
        String expResult = "mp3";
        String result = Utils.getExtension(f);
        assertEquals(expResult, result);
    }
    
}
